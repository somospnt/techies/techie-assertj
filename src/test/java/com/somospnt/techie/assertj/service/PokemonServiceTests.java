package com.somospnt.techie.assertj.service;

import com.somospnt.techie.assertj.TechieAssertJApplicationTests;
import com.somospnt.techie.assertj.builder.PokemonBuilder;
import com.somospnt.techie.assertj.domain.Pokemon;
import com.somospnt.techie.assertj.domain.TipoPokemon;
import java.util.List;
import javax.transaction.Transactional;
import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Transactional
public class PokemonServiceTests extends TechieAssertJApplicationTests {

    @Autowired
    private PokemonService pokemonService;

    @Test
    public void buscarPorNombre_conNombreValido_retornaPokemonCorrespondiente() {
        PokemonBuilder.pikachu().build(entitiyManager);
        PokemonBuilder.charizard().build(entitiyManager);
        PokemonBuilder.articuno().build(entitiyManager);

        Pokemon pokemonEncontrado = pokemonService.buscarPorNombre("Pikachu");

        assertThat(pokemonEncontrado).isNotNull();
        assertThat(pokemonEncontrado.getNombre()).isEqualTo("Pikachu");
        assertThat(pokemonEncontrado.getId()).isEqualTo(25L);
        assertThat(pokemonEncontrado.getNivel()).isEqualTo(88);
        assertThat(pokemonEncontrado.getTipoPrimario()).isEqualTo(TipoPokemon.ELECTRICO);
        assertThat(pokemonEncontrado.getTipoSecundario()).isNull();

        //Buscar que sea el mismo
    }

    @Test
    public void buscarPorNivelMayorA_conNivel50_retornaPokemonsConNivelMayorA50() {
        PokemonBuilder.pikachu().build(entitiyManager);
        PokemonBuilder.venusaur().build(entitiyManager);
        PokemonBuilder.mew().build(entitiyManager);

        List<Pokemon> pokemons = pokemonService.buscarPorNivelMayorA(50);

        assertThat(pokemons).hasSize(2);
        pokemons.forEach(pokemon -> {
            assertThat(pokemon.getNivel()).isGreaterThan(50);
        });

        //Que todos satisfagan esa condicion
    }

    @Test
    public void buscarPorTipo_conTipoFuego_retornaPokemonsDelTipo() {
        PokemonBuilder.pikachu().build(entitiyManager);
        PokemonBuilder.charizard().build(entitiyManager);
        PokemonBuilder.houndoom().build(entitiyManager);

        List<Pokemon> pokemons = pokemonService.buscarPorTipo(TipoPokemon.FUEGO);

        pokemons.forEach(pokemon -> {
            assertThat(
                    pokemon.getTipoPrimario().equals(TipoPokemon.FUEGO)
                    || pokemon.getTipoSecundario().equals(TipoPokemon.FUEGO)
            ).isTrue();
        });
        assertThat(pokemons).hasSize(2);

        //Que cumpla alguna de las dos condiciones
    }

}
