package com.somospnt.techie.assertj.builder;

import com.somospnt.techie.assertj.domain.Pokemon;
import com.somospnt.techie.assertj.domain.TipoPokemon;
import com.somospnt.test.builder.AbstractPersistenceBuilder;

public class PokemonBuilder extends AbstractPersistenceBuilder<Pokemon> {

    private PokemonBuilder() {
        instance = new Pokemon();
    }

    public static PokemonBuilder charizard() {
        PokemonBuilder builder = new PokemonBuilder();
        builder.instance.setId(6L);
        builder.instance.setNivel(55);
        builder.instance.setNombre("Charizard");
        builder.instance.setTipoPrimario(TipoPokemon.FUEGO);
        builder.instance.setTipoSecundario(TipoPokemon.VOLADOR);
        return builder;
    }

    public static PokemonBuilder pikachu() {
        PokemonBuilder builder = new PokemonBuilder();
        builder.instance.setId(25L);
        builder.instance.setNivel(88);
        builder.instance.setNombre("Pikachu");
        builder.instance.setTipoPrimario(TipoPokemon.ELECTRICO);
        return builder;
    }

    public static PokemonBuilder articuno() {
        PokemonBuilder builder = new PokemonBuilder();
        builder.instance.setId(144L);
        builder.instance.setNivel(70);
        builder.instance.setNombre("Articuno");
        builder.instance.setTipoPrimario(TipoPokemon.HIELO);
        builder.instance.setTipoSecundario(TipoPokemon.VOLADOR);
        return builder;
    }

    public static PokemonBuilder mew() {
        PokemonBuilder builder = new PokemonBuilder();
        builder.instance.setId(151L);
        builder.instance.setNivel(35);
        builder.instance.setNombre("Mew");
        builder.instance.setTipoPrimario(TipoPokemon.PSIQUICO);
        return builder;
    }

    public static PokemonBuilder venusaur() {
        PokemonBuilder builder = new PokemonBuilder();
        builder.instance.setId(3L);
        builder.instance.setNivel(83);
        builder.instance.setNombre("Venusaur");
        builder.instance.setTipoPrimario(TipoPokemon.PLANTA);
        builder.instance.setTipoSecundario(TipoPokemon.VENENO);
        return builder;
    }

    public static PokemonBuilder houndoom() {
        PokemonBuilder builder = new PokemonBuilder();
        builder.instance.setId(229L);
        builder.instance.setNivel(50);
        builder.instance.setNombre("Houndoom");
        builder.instance.setTipoPrimario(TipoPokemon.SINIESTRO);
        builder.instance.setTipoSecundario(TipoPokemon.FUEGO);
        return builder;
    }
}
