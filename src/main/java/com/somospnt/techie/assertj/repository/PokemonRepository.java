package com.somospnt.techie.assertj.repository;

import com.somospnt.techie.assertj.domain.Pokemon;
import com.somospnt.techie.assertj.domain.TipoPokemon;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PokemonRepository extends JpaRepository<Pokemon, Long> {

    List<Pokemon> findByNombre(String nombre);

    List<Pokemon> findByNivelGreaterThan(int nivel);

    @Query("SELECT p FROM Pokemon p WHERE p.tipoPrimario = :tipo or p.tipoSecundario = :tipo")
    List<Pokemon> findByTipoPrimarioOrTipoSecundario(TipoPokemon tipo);
}
