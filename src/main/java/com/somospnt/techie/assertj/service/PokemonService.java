package com.somospnt.techie.assertj.service;

import com.somospnt.techie.assertj.repository.PokemonRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.somospnt.techie.assertj.domain.Pokemon;
import com.somospnt.techie.assertj.domain.TipoPokemon;
import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class PokemonService {

    private final PokemonRepository pokemonRepository;

    public Pokemon buscarPorNombre(String nombre) {
        List<Pokemon> pokemons = pokemonRepository.findByNombre(nombre);

        return pokemons.stream().findFirst().orElse(new Pokemon());
    }

    public List<Pokemon> buscarPorNivelMayorA(int nivel) {
        List<Pokemon> pokemons = pokemonRepository.findByNivelGreaterThan(nivel);

        return pokemons;
    }

    public List<Pokemon> buscarPorTipo(TipoPokemon tipoPokemon) {
        List<Pokemon> pokemons = pokemonRepository.findByTipoPrimarioOrTipoSecundario(tipoPokemon);

        return pokemons;
    }

}
