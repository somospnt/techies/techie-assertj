package com.somospnt.techie.assertj.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Pokemon {

    @Id
    private Long id;
    private String nombre;
    @Enumerated(EnumType.STRING)
    private TipoPokemon tipoPrimario;
    @Enumerated(EnumType.STRING)
    private TipoPokemon tipoSecundario;
    private int nivel;

}
