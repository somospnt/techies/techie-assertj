package com.somospnt.techie.assertj.domain;

public enum TipoPokemon {
    FUEGO,
    AGUA,
    PLANTA,
    ELECTRICO,
    NORMAL,
    VOLADOR,
    BICHO,
    FANTASMA,
    PSIQUICO,
    LUCHA,
    ROCA,
    HIELO,
    DRAGON,
    VENENO,
    ACERO,
    SINIESTRO,
    HADA

}
