package com.somospnt.techie.assertj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieAssertJApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieAssertJApplication.class, args);
	}

}
